# Azure

### Preferences:

* we create new resource group named "AnsibleYumRepo"
* we create new VM named "AnsibleYumRepo"
* our admin user in new VM is named "itech5"
* file on your workstation for storing SSH key is named "yumrepo.ssh.private.key"
* assume IP of new VM is 11.22.33.44

### Azure CLI commands:

* Create new resource group:  
  `az group create --name AnsibleYumRepo --location westeurope`
* Create new VM:  
  `az vm create --resource-group AnsibleYumRepo --name AnsibleYumRepo --image CentOS --admin-username itech5 --generate-ssh-keys`
* Save IP-address of new VM reported to console (for example, 11.22.33.44)
* Print content of generated SSH key to console and save it from console to local file "yumrepo.ssh.private.key" (this file must be readable only for you!):  
  `cat ~/.ssh/id_rsa`
* Enable HTTPS in Azure firewall:  
  `az vm open-port --resource-group AnsibleYumRepo --name AnsibleYumRepo --port 443`

### Deploy and run Ansible:

* `cd ~/clouddrive`
* `git clone https://bitbucket.org/evseev/ansibleyumrepo`
* ..your Bitbucket login and password are asked here!
* `ansible-playbook -i 11.22.33.44, -u itech5 -b yumrepo.yml`

### Deploy RPM:

* `scp -i yumrepo.ssh.private.key file.rpm itech5@11.22.33.44:`
* `ssh -i yumrepo.ssh.private.key itech5@11.22.33.44 /usr/local/sbin/yumrepo-addpkg.sh file.rpm`
* ..press Enter for password
