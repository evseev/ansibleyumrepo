## Run ansible:

* Run: `ansible-playbook -i remote.host.addr, -u root yumrepo.yml`
* Take notice on comma after hostname, it's required!

## Web users:

* Edit **yumrepo-webusers.auth** file locally BEFORE running Ansible
* Or edit **/etc/nginx/conf.d/yumrepo-webusers.auth** on remote host (Ansible will NOT overwrite it).

## Publish packages:

* Put RPM file to remote.host
* Run: `yumrepo-addpkg.sh /path/to/file.rpm`
* Press Enter when it asks for password

## Access repo from client:

* Create file */etc/yum.repos.d/YumRepo.repo* (replace **xx** to actual username and password)
```
[YumRepo]
name=Our YUM repository
enabled=1
baseurl=https://remote.host.addr
sslverify=0
username=xx
password=xx
gpgkey=file:///etc/pki/rpm-gpg/yumrepo-rpm.public.key
gpgcheck=1
```
* Run: `curl -sS -k -o /etc/pki/rpm-gpg/yumrepo-rpm.public.key https://xx:xx@remote.host.addr/yumrepo-rpm.public.key`
* Run: `rpm --import /etc/pki/rpm-gpg/yumrepo-rpm.public.key`
* Run: `yum update`
